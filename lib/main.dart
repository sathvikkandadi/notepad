import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {
  runApp(
    MyApp()

  );
}
class MyApp extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Center(child: Text('Notes')),
          backgroundColor: Colors.lightBlueAccent,
        ),

    body: Column(
    mainAxisSize: MainAxisSize.min,
    children: <Widget>[
    RawMaterialButton(
    //style: style,
    onPressed: () {},
      fillColor: Colors.yellow,
      constraints: BoxConstraints.tightFor(height: 100, width: 2000),

    child: Row(
      children:[
        const Text('Subject 1'),
      ],
    ),

    ),
    const SizedBox(height: 1),
      RawMaterialButton(
        //style: style,
        onPressed: () {},
        fillColor: Colors.yellow,
        constraints: BoxConstraints.tightFor(height: 100, width: 2000),

        child: Row(
          children:[
            const Text('Subject 2'),
          ],
        ),

      ),
      const SizedBox(height: 1),
      RawMaterialButton(
        //style: style,
        onPressed: () {},
        fillColor: Colors.yellow,
        constraints: BoxConstraints.tightFor(height: 100, width: 2000),

        child: Row(
          children:[
            const Text('Subject 3'),
          ],
        ),

      ),
      const SizedBox(height: 1),
      RawMaterialButton(
        //style: style,
        onPressed: () {},
        fillColor: Colors.yellow,
        constraints: BoxConstraints.tightFor(height: 100, width: 2000),

        child: Row(
          children:[
            const Text('Subject 4'),
          ],
        ),

      ),
      const SizedBox(height: 1),
      RawMaterialButton(
        //style: style,
        onPressed: () {},
        fillColor: Colors.yellow,
        constraints: BoxConstraints.tightFor(height: 100, width: 2000),

        child: Row(
          children:[
            const Text('Subject 5'),
          ],
        ),

      ),
    ],
    ),

        floatingActionButton: FloatingActionButton(
          onPressed: () {


          },
          child: const Icon(Icons.add),
          backgroundColor: Colors.red,
        ),


      ),
    );
  }
}
