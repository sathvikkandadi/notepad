import 'package:flutter/rendering.dart';

void main() {
  runApp(
      MyApp()

  );
}
class MyApp extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Center(child: Text('Notes')),
          backgroundColor: Colors.lightBlueAccent,
        ),

        body: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ElevatedButton(
              //style: style,
              onPressed: () {},

              child: Row(
                children:[
                  const Text('Subject 1'),
                ],
              ),

            ),
            const SizedBox(height: 1),
            ElevatedButton(
              //style: style,
              onPressed: () {},
              child: Row(
                children: [
                  const Text('Subject 2'),
                ],
              ),
            ),
          ],
        ),

        floatingActionButton: FloatingActionButton(
          onPressed: () {


          },
          child: const Icon(Icons.add),
          backgroundColor: Colors.red,
        ),


      ),
    );
  }
}